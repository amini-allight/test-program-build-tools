#!/bin/sh
brew install git gcc cmake make p7zip sdl2
git clone "https://amini-allight:$BUILD_SWARM_PAC@gitlab.com/amini-allight/test-program.git"
cd test-program
mkdir -p build
cd build
export C_INCLUDE_PATH=$C_INCLUDE_PATH:/usr/local/include
export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/usr/local/include
export LIBRARY_PATH=$LIBRARY_PATH:/usr/local/lib
cmake ..
make
cd ../..
7z a "$BUILD_SWARM_WORKER_ID.zip" test-program/bin/test-program
echo "$BUILD_SWARM_WORKER_ID.zip" > file-name
