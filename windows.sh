#!/bin/sh
pacman -S --noconfirm git $MINGW_PACKAGE_PREFIX-gcc $MINGW_PACKAGE_PREFIX-cmake ninja p7zip $MINGW_PACKAGE_PREFIX-SDL2
git clone "https://amini-allight:$BUILD_SWARM_PAC@gitlab.com/amini-allight/test-program.git"
cd test-program
mkdir -p build
cd build
cmake ..
ninja
cd ../..
7z a "$BUILD_SWARM_WORKER_ID.zip" test-program/bin/test-program
echo "$BUILD_SWARM_WORKER_ID.zip" > file-name
