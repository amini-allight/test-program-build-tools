#!/bin/sh
sudo apt install -y git g++ cmake p7zip-full libsdl2-dev
git clone "https://amini-allight:$BUILD_SWARM_PAC@gitlab.com/amini-allight/test-program.git"
cd test-program
mkdir -p build
cd build
cmake ..
make
cd ../..
7z a "$BUILD_SWARM_WORKER_ID.zip" test-program/bin/test-program
echo "$BUILD_SWARM_WORKER_ID.zip" > file-name
